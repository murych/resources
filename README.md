# Разные разности

## Вступление

Здесь будут собраны ссылки на различные ресурсы и прогарммы, имеющие отношение к Python, LaTeX, Markdown, списки полезных программ и прочее. Матераил собирается из разных источников, ссылки на авторов бережно сохраняются.

-------
## Online
 - [Список софта]()

## Windows
 - [Список софта](https://bitbucket.org/murych/resources/src/49cb0746c52aa6e8176bbe2c4dfd1cbd958def95/windows/soft.md?at=master)
 - [Кастомизация интерфейса]()
 - [Кастомизация интерфейса Firefox](https://bitbucket.org/murych/resources/src/49cb0746c52aa6e8176bbe2c4dfd1cbd958def95/customizing/firefox.md?at=master)

## Android
- [Список софта](https://bitbucket.org/murych/resources/src/49cb0746c52aa6e8176bbe2c4dfd1cbd958def95/android/soft_android.md?at=master)

## Python
 - [Обучающие ресурсы]()
 - [Github репозитории с материалами]()

## LaTeX
 - [Обзор редакторов для работы в LaTeX](https://bitbucket.org/murych/resources/src/49cb0746c52aa6e8176bbe2c4dfd1cbd958def95/latex/latex_editors.md?at=master) ([оригинальная статья](http://mydebianblog.blogspot.ru/2013/02/latex-editors-and-integrated-latex.html))