# IDEs
- [Python IDLE](https://www.python.org/downloads/)
- [JetBrains PyCharm Community Edition](http://www.jetbrains.com/pycharm/) - `IDE` для `python` от JetBrains
- [SciTE](http://www.scintilla.org/SciTE.html)
- [Spyder IDE](https://github.com/spyder-ide) - The Scientific PYthon Development EnviRonment
- [Eric IDE](http://eric-ide.python-projects.org/)

# Сборки
- [Python(x,y)](https://code.google.com/p/pythonxy/) - готовая сборка `python` с библиотеками для научных вычислений и средами разработки
- [WinPython](https://sourceforge.net/projects/winpython/?source=directory)