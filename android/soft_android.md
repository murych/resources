# Android

## Дисклеймер
*Все программное обеспечение, фигурируещее в данном списке - бесплатно или условно-бесплатно*

## Интернет
- [Firefox](https://f-droid.org/repository/browse/?fdfilter=firefox&fdid=org.mozilla.firefox)
- [Opera](https://play.google.com/store/apps/details?id=com.opera.browser&hl=en), [Opera Mini](https://play.google.com/store/apps/details?id=com.opera.mini.native&hl=en)
- [Kate Mobile](https://play.google.com/store/apps/details?id=com.perm.kate_new_3&hl=en)
- [2ch browser](https://play.google.com/store/apps/details?id=com.musenkishi.wally)
- [Dashchan](https://2ch.hk/mobi/res/456058.html)
- [feedly](https://play.google.com/store/apps/details?id=com.devhd.feedly)
- [Pocket](https://play.google.com/store/apps/details?id=com.ideashower.readitlater.pro)
- [Facebook Lite](http://www.apkmirror.com/apk/facebook-2/lite/facebook-lite-1-4-0-6-14-apk/)
- [Gmail](https://play.google.com/store/apps/details?id=com.google.android.gm&hl=en)
- [Facebook Messenger](https://play.google.com/store/apps/details?id=com.facebook.orca)
- [Telegram](https://f-droid.org/repository/browse/?fdfilter=telegram&fdid=org.telegram.messenger)
- [Yaaic](https://f-droid.org/repository/browse/?fdfilter=yaaic&fdid=org.yaaic)
- [Skype](https://play.google.com/store/apps/details?id=com.skype.raider)
- [Viber](https://play.google.com/store/apps/details?id=com.viber.voip)

## Образование + Офис
- [handyCalc](https://play.google.com/store/apps/details?id=org.mmin.handycalc&hl=en)
- [Решатель квадратных уравнений](https://play.google.com/store/apps/details?id=org.me.kvur)
- [FBReader](https://f-droid.org/repository/browse/?fdfilter=fbreader&fdid=org.geometerplus.zlibrary.ui.android)
- [PocketBook](https://play.google.com/store/apps/details?id=com.obreey.reader)
- [Adobe Reader](https://play.google.com/store/apps/details?id=com.adobe.reader)
- [Wikipedia](https://f-droid.org/repository/browse/?fdfilter=wikipedia&fdid=org.wikipedia)
- [Writeily](https://play.google.com/store/apps/details?id=com.jmartin.writeily)
- [Tasks](https://play.google.com/store/apps/details?id=org.tasks)
- [Plain.txt](https://play.google.com/store/apps/details?id=uk.co.jasonfry.android.apps.Plaintxt)
- [CamScanner](https://play.google.com/store/apps/details?id=com.intsig.camscanner)
- [Google Translate](https://play.google.com/store/apps/details?id=com.google.android.apps.translate&hl=en)
- [Offline Dictionaries](https://play.google.com/store/apps/details?id=fr.nghs.android.dictionnaires&hl=en)
- [Физика](https://play.google.com/store/apps/details?id=ru.wildwarriors.physics)
- [Elementary](https://f-droid.org/repository/browse/?fdfilter=elementary&fdid=com.ultramegatech.ey)
- [Electro Droid](https://play.google.com/store/apps/details?id=it.android.demi.elettronica&hl=en)
- [iCircuit](https://play.google.com/store/apps/details?id=com.kruegersystems.circuitdroid&hl=en) (**155р.**)

## Медиа
- [AIMP](https://play.google.com/store/apps/details?id=com.aimp.player)
- [Shuttle](https://play.google.com/store/apps/details?id=another.music.player&hl=en)
- [Gramophone](https://play.google.com/store/apps/details?id=com.kabouzeid.gramophone&hl=en)
- [DeviantArt](https://play.google.com/store/apps/details?id=com.deviantart.android.damobile)
- [Instagram](https://play.google.com/store/apps/details?id=com.instagram.android)
- [VLC](https://play.google.com/store/apps/details?id=org.videolan.vlc.betav7neon)
- [QuickLyric](https://f-droid.org/repository/browse/?fdfilter=quick&fdid=be.geecko.QuickLyric&fdpage=2)

## Разработка
- [OctoDroid](https://f-droid.org/repository/browse/?fdfilter=octodroid&fdid=com.gh4a)
- [Bitbeaker](https://play.google.com/store/apps/details?id=fi.iki.kuitsi.bitbeaker&hl=en)
- [Git](https://play.google.com/store/apps/details?id=com.romanenco.gitt)
- [JotterPad X](http://4pda.ru/forum/index.php?showtopic=553217)
- [Pastebin for Android](https://play.google.com/store/apps/details?id=com.jmz.pastedroidapp)
- [RegexPal](https://play.google.com/store/apps/details?id=com.andkorsh.regexpal)
- [ServerAuditor](https://play.google.com/store/apps/details?id=com.server.auditor.ssh.client)
- [TeamViewer](https://play.google.com/store/apps/details?id=com.teamviewer.teamviewer.market.mobile)
- [VNC Viewer](https://play.google.com/store/apps/details?id=com.realvnc.viewer.android)

## Утилиты
- [F-Droid](https://f-droid.org/)
- [/system/app mover](https://f-droid.org/repository/browse/?fdid=de.j4velin.systemappmover) (**root**)
- [Total Commander](https://play.google.com/store/apps/details?id=com.ghisler.android.TotalCommander)
- [Alarm Clock](https://f-droid.org/repository/browse/?fdid=za.co.neilson.alarm&fdpage=2)
- [Dropbox](https://play.google.com/store/apps/details?id=com.dropbox.android)
- [Papyrus](https://play.google.com/store/apps/details?id=com.steadfastinnovation.android.projectpapyrus)
- [gStrings](https://play.google.com/store/apps/details?id=org.cohortor.gstrings)


## Интерфейс
- [Smart Launcher Pro](https://play.google.com/store/apps/details?id=ginlemon.flowerfree&hl=en)
- [Voxel](https://play.google.com/store/apps/details?id=com.benx9.palmtree)
- [Google Keyboard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin)
- [Wally](https://play.google.com/store/apps/details?id=com.musenkishi.wally)
- [acDisplay](https://f-droid.org/repository/browse/?fdid=com.achep.acdisplay)

## Яндекс
- [Метро](https://play.google.com/store/apps/details?id=ru.yandex.metro)
- [Расписание](https://play.google.com/store/apps/details?id=ru.yandex.rasp)
- [Карты](https://play.google.com/store/apps/details?id=ru.yandex.yandexmaps)
